package org.mentorship.controller;

import org.mentorship.model.Article;
import org.mentorship.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Maks on 26.11.2016.
 */
@RestController
@RequestMapping("/article")
public class ArticleRestController {

    @Autowired
    private ArticleRepository articleRepository;


    @GetMapping("/articles")
    public List<Article> getAllArticles() {
        return articleRepository.findAll();
    }

    @CrossOrigin("*")
    @PutMapping("/article")
    public Article updateArticle(@RequestBody Article article) {
        return articleRepository.save(article);
    }

    @DeleteMapping("/article")
    public void deleteAtricle(@RequestBody Article article) {
        articleRepository.delete(article);
    }

    @PostMapping("/article")
    public Article addArticle(@RequestBody Article article){
        return articleRepository.save(article);
    }

    @GetMapping("/article/{id}")
    public Article getArticle(@PathVariable String id){
        return articleRepository.findOne(id);
    }
}
