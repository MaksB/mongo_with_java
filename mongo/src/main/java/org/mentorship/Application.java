package org.mentorship;

import org.mentorship.model.Article;
import org.mentorship.model.Author;
import org.mentorship.model.Comment;
import org.mentorship.model.Tag;
import org.mentorship.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.Date;
import java.util.stream.Stream;

/**
 * Created by Maksym_Borskyi on 11/16/2016.
 */
@SpringBootApplication
public class Application implements CommandLineRunner {


    @Autowired
    private ArticleRepository articleRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        articleRepository.deleteAll();
        Stream.iterate(1, i -> ++i).limit(10).forEach(i -> {
            Author author = new Author();
            author.setFitstName("Maks");
            author.setSecondName("Borskyi");

            Comment comment = new Comment();
            comment.setDate(new Date());
            comment.setText("Good article! " + i);

            Tag tag = new Tag();
            tag.setTagName("Sport");


            Article article = new Article();
            article.setText("text № " + i);
            article.setDate(new Date());
            article.setAuthor(author);
            article.setComments(Arrays.asList(comment, comment));
            article.setTags(Arrays.asList(tag));
            articleRepository.save(article);
        });

        System.out.println(articleRepository.findByCommentsText("Good article! 1"));
        System.out.println(articleRepository.findAll());


    }
}
