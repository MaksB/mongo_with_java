package org.mentorship.model;

import lombok.Data;

/**
 * Created by Maksym_Borskyi on 11/18/2016.
 */
@Data
public class Author {

    private String fitstName;
    private String secondName;
}
