package org.mentorship.model;

import lombok.Data;

import java.util.Date;

/**
 * Created by Maksym_Borskyi on 11/18/2016.
 */
@Data
public class Comment {

    private String text;
    private Date date;
}
