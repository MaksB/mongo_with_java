package org.mentorship.model;

import lombok.Data;

/**
 * Created by Maksym_Borskyi on 11/18/2016.
 */
@Data
public class Tag {

    private String tagName;
}
