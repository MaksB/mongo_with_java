package org.mentorship.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.List;

/**
 * Created by Maksym_Borskyi on 11/18/2016.
 */
@Data
public class Article {

    @Id
    private String id;
    private String text;
    private Author author;
    private List<Comment> comments;
    private List<Tag> tags;
    private Date date;

}
