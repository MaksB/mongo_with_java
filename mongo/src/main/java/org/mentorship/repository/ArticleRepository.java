package org.mentorship.repository;

import org.mentorship.model.Article;
import org.mentorship.model.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * Created by Maksym_Borskyi on 11/18/2016.
 */
@Component
@Repository
public interface ArticleRepository extends MongoRepository<Article, String>{

    Article findByCommentsText(String text);
}
